package lectures.part3fp

import scala.util.Random

/**
  * User: gert
  * Date: 2019/01/22
  * Time: 8:13 AM
  */
object Sequences extends App {

  // Seq
  val aSequence = Seq(1, 3, 2, 4)
  println(aSequence)
  println(aSequence.reverse)
  println(aSequence(2))
  println(aSequence ++ Seq(5, 6, 7))
  println(aSequence.sorted)

  // Ranges
  val aRange: Seq[Int] = 1 to 10
  aRange.foreach(println)

  (1 to 10).foreach(x => println("Hello"))

  // lists
  val aList = List(1, 2, 3)
  val prepended = 42 :: aList
  val anotherPrepended = 42 +: aList
  println(prepended)
  println(anotherPrepended)

  val appended = aList :+ 89
  println(appended)

  val apples5 = List.fill(5)("apple")
  println(apples5)
  println(aList.mkString("-|-"))

  // arrays
  val numbers = Array(1, 2, 3, 4)
  val threeElements = Array.ofDim[Int](3)
  println(threeElements)
  threeElements.foreach(println)

  // mutation
  numbers(2) = 0 // syntax sugar for numbers.update(2,0)
  println(numbers.mkString(" "))

  // arrays and sequences
  val numbersSeq: Seq[Int] = numbers // implicit conversion
  println(numbersSeq)


  // vectors
  val vectors: Vector[Int] = Vector(1, 2, 3)
  println(vectors)

  // vectors vs lists
  val maxRuns = 1000
  val maxCapacity = 1000000

  def getWriteTime(collection: Seq[Int]): Double = {
    val r = new Random
    val times = for {
      it <- 1 to maxRuns
    } yield {
      val currentTime = System.nanoTime()
      collection.updated(r.nextInt(maxCapacity), r.nextInt()) // operation
      System.nanoTime() - currentTime
    }

    times.sum * 1.0 / maxRuns
  }

  val numbersList = (1 to maxCapacity).toList
  val numbersVector = (1 to maxCapacity).toVector

  // adv. keeps reference to tail
  // disadv. updating an element in the middle takes long
  println(getWriteTime(numbersList))
  // adv. depth of the tree is small
  // disadv. needs to replace an entire 32-element chunk
  println(getWriteTime(numbersVector))

}
