package lectures.part1Basics

import scala.annotation.tailrec

/**
  * User: kobusa
  * Date: 2018/12/04
  * Time: 7:23 AM
  */
object Recursion extends App {

  def factorial(n: Int): Int =
    if (n <= 1) 1
    else {
      println("Computing factorial of " + n + " - I first need factorial of " + (n - 1))
      val result = n * factorial(n - 1)
      println("Computed the factorial of " + n)
      result
    }

  println(factorial(10))
  //  println(factorial(5000))

  def anotherFactorial(n: Int): BigInt = {
    @tailrec
    def factHelper(x: Int, accumulator: BigInt): BigInt =
      if (x <= 1) accumulator
      else factHelper(x - 1, x * accumulator) // TAIL RECURSION = use the recursive call as the LAST expression

    factHelper(n, 1)

  }

  println(anotherFactorial(20000))

  // WHEN YOU NEED LOOPS, USE _TAIL_ RECURSION.

  /*
    Do these exercises using tail recursion...

    1. Concatenate a string n times tail recursive
    2. IsPrime function tail recursive
    3. Fibonacci function, tail recursive
   */

  // 1.
  @tailrec
  def concatenateTailrec(aString: String, n: Int, accumulator: String): String =
    if (n <= 0) accumulator
    else concatenateTailrec(aString, n - 1, aString + accumulator)

  println(concatenateTailrec("kobus", 5, ""))

  // 2.
  def isPrime(n: Int): Boolean = {
    @tailrec
    def isPrimeTailRec(t: Int, isStillPrime: Boolean): Boolean =
      if (!isStillPrime) false
      else if (t <= 1) true
      else isPrimeTailRec(t - 1, n % t != 0 && isStillPrime)

    isPrimeTailRec(n / 2, true)

  }

  println(isPrime(2003))
  println(isPrime(629))


  // 3.
  def fibonacci(n: Int): Int = {

    @tailrec
    def fibonacciTailRec(x: Int, last: Int, previous: Int): Int =
      if (x >= n) last
      else fibonacciTailRec(x + 1, last + previous, last)

    if (n <= 2) 1
    else fibonacciTailRec(2, 1, 1)

  }

  println(fibonacci(8))

}
