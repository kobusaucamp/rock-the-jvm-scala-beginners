package lectures.part1Basics

/**
  * User: kobusa
  * Date: 2018/12/04
  * Time: 1:09 PM
  */
object DefaultArgs extends App {

  def trFactorial(n: Int, acc: Int = 1): Int =
    if (n <= 1) acc
    else trFactorial(n-1, n*acc)

  //Any except the leading parameter can be omitted
  val fact10 = trFactorial(10)

  def savePicture(format: String =  "jpg", width: Int = 1920, height: Int = 1080): Unit = println("saving picture")
  savePicture("bmp")

  /*
    Solutions to this problem
    1. pass in every leading argument   // savePicture("bmp")
    2. name the arguments          // savePicture(width = 800)
   */

  // A side effect of the naming of parameters means you can change the order in which you pass in the arguments
  savePicture(height = 600, width = 800, format = "bmp")
}
