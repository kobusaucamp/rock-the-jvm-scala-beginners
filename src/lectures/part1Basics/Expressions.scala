package lectures.part1Basics

object Expressions extends App {

  val x = 1 + 2   //EXPRESSION
  println(x)

  println(2 + 3 * 4)
  // + - * / & | ^ << >> >>>(right shift with zero extension)

  println(1 == x)
  // == != > >= < <=

  println(!(1 == x))
  // ! && ||

  var aVariable = 2
  aVariable += 3    // also works with -= *= /=  (only works with variables) ... side affects
  println(aVariable)

  // Instructions (tell the computer to DO) vs Expressions (Something that has a Value and/or a type)

  // example IF expression
  val aCondition = true
  val aConditionValue = if(aCondition) 5 else 3 // IF returns a value, that's why it/'s called an IF expression
  println(aConditionValue)
  println(if(aCondition) 5 else 3)

  // Loops  (DISCOURAGE THE USE OF LOOPS)

  //DON'T EVER WRITE THIS AGAIN

  // while loops are also side effects => returning Unit
  var i = 0
  val aWhile = while (i < 10) {
    println(i)
     i += 1
  }

  // EVERYTHING IN SCALA IS AN EXPRESSION

  val aWeirdValue = (aVariable = 3)   // Unit === void     (Don't return anything meaningful) always returns "()"
                                      // side affects (assigning values to variables) in scala are expressions returning Unit...
  println(aWeirdValue)

  // side effects: println(), whiles, reassigning of vars

  // Code blocks    -      Special kind of Expressions

  val aCodeBlock = {
    val y = 2
    val z = y + 1

    if (z > 2) "hello" else "goodbye"   // The value of the code block is the value of the last Expression
  }
  println(aCodeBlock)

  // 1. difference between "hello world" vs println("hello world")?
  // "hello world" is an expression of type String and println("hello world") is an instruction which has the side affect of printing out the string (which returns unit)

  // 2.

  val someValue = {
    2 < 3
  }    // the value of the code block is true (a boolean)
  println(someValue)

  val someOtherValue = {
    if (someValue) 239 else 986
    42
  }     // the value of the code block is an Int of value 42
  println(someOtherValue)
}
