package lectures.part1Basics

object ValuesVariablesTypes extends App {

  // vals

  // VALS ARE IMMUTABLE
  // TYPES OF VAL ARE OPTIONAL - COMPILER CAN INFER TYPES

  val x = 42   //like a constant, it cannot be changed
  println(x)

  // types

  val aString: String = "hello"
  val anotherString = "goodbye"

  val aBoolean: Boolean = false    //true or false
  val aChar: Char = 'a'
  val anInt: Int = x    // we assign another val to it, it his case the val x we assigned on the first line
  val aShort: Short = 32767    // Int of half the presentation size, so, 2 bytes (16-bit) instead of 4 bytes (32-bit) (max of 32767)
  val aLong: Long = 35687654L
  val aFloat: Float = 2.0f
  val aDouble : Double = 3.14



  // variables

  // VARS ARE MUTABLE
  var aVariable: Int = 4
  aVariable = 5    //side affects


}
