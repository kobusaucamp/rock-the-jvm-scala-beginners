package lectures.part1Basics

/**
  * User: kobusa
  * Date: 2018/12/04
  * Time: 1:27 PM
  */
object StringOps extends App {

  val str: String = "Hello, I am learning Scala"

  println(str.charAt(2))
  println(str.substring(7, 11))
  println(str.split(" ").toList)
  println(str.startsWith("Hello"))
  println(str.replace(" ", "-"))
  println(str.toLowerCase())
  println(str.length)

  val aNumberString = "2"
  val aNumber = aNumberString.toInt

  println('a' +: aNumberString :+ 'z)   // +: prepending       :+ appending      //Scala specific
  println(str.reverse)
  println(str.take(2))

  // Scala-specific  ->   String interpolators

  // S-interpolators
  val name = "David"
  val age = 12
  val greetings = s"Hello, my name is $name and I am $age years old"
  val anotherGreeting = s"Hello, my name is $name and i will be turning ${age + 1} years old"
  println(anotherGreeting)


  // F-interpolators     // For formatted strings, similar to printf. Can also check for type correctness in the value they expand.
  val speed = 1.2f
  val myth = f"$name can eat $speed%2.2f burgers per minute"
  println(myth)

  /*val x = 1.1f
  val st = f"$x%3d"*/

  // raw-interpolators
  println(raw"This is a \n newline")     // Raw interpolated strings ignores escaped characters inside raw characters in the string
  val escaped = "This is a \n newline"
  println(escaped)
  println(raw"$escaped")     //Injected variables DO get escaped


}
