package lectures.part1Basics

/**
  * User: kobusa
  * Date: 2018/12/03
  * Time: 8:01 AM
  */
object Functions extends App {

  // NORMAL FUNCTIONS DO NOT NEED RETURN TYPES
  def aFunction(a: String, b: Int): String = {
    a + " " + b
  }

  println(aFunction("hello", 3))

  def aParameterlessFunction(): Int = 42

  println(aParameterlessFunction())
  println(aParameterlessFunction)


  // WHEN YOU NEED LOOPS, USE RECURSION.

  // RECURSIVE FUNCTIONS NEED RETURN TYPES
  def aRepeatedFunction(aString: String, n: Int): String = {
    if (n == 1) aString
    else aString + aRepeatedFunction(aString, n - 1)
  }

  println(aRepeatedFunction("hello", 3))


  //FUNCTIONS CAN RETURN THE UNIT TYPE
  def aFunctionWithSideAffect(sString: String): Unit = println(sString)

  def aBigFunction(n: Int): Int = {
    def aSmallerFunction(a: Int, b: Int): Int = a + b

    aSmallerFunction(n, n - 1)
  }

  /*
  1. A greeting function (name, age) => "Hi, my name is $name and I am $age years old."
  2. Factorial function 1 * 2 * 3 * ... * n    (recursive)
  3. A Fibonacci function
      f(1) = 1
      f(2) = 1
      f(n) = f(n - 1) + f( n - 2)
  4. Test if a number is prime
   */

  // 1.
  def greetingFunction1(name: String, age: Int): Unit =
    println("Hi, my name is " + name + " and I am " + age + " years old.")


  greetingFunction1("Kobus", 46)

  def greetingFunction2(name: String, age: Int): String =
    "Hi, my name is " + name + " and I am " + age + " years old."


  println(greetingFunction2("Kobus", 46))

  // 2.
  def factorial(n: Int): Int =
    if (n - 1 > 0) factorial(n - 1) * n
    else n


  println(factorial(5))

  // 3.
  def fibonacci(n: Int): Int =
    if (n <= 2) 1
    else fibonacci(n - 1) + fibonacci(n - 2)

  println(fibonacci(8))

  // 4.
  def isPrime(n: Int): Boolean = {

    def isPrimeUntil(t: Int): Boolean =
      if (t <= 1) true
      else n % t != 0 && isPrimeUntil(t - 1)

    isPrimeUntil(n / 2)

  }

  println(isPrime(37))
  println(isPrime(2003))
  println(isPrime(37 * 17))
}
