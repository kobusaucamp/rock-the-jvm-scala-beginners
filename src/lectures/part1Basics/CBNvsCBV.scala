package lectures.part1Basics

/**
  * User: kobusa
  * Date: 2018/12/04
  * Time: 12:00 PM
  */
object CBNvsCBV extends App {

  // When the function is called and the value x is evaluated it basically passes the value of System.nanoTime() into the function
  def calledByValue(x: Long): Unit = {
    println("by value: " + x) // by value: 1059763882227237
    println("by value: " + x) // by value: 1059763882227237
  }

  /*When the function is called and the value x is evaluated it basically passes the function System.nanoTime() into the function,
  so, it gets evaluated when it needs to be used*/
  def calledByName(x: => Long): Unit = {
    println ("by name: " + x)  // by name: 1059763968412167
    println ("by name: " + x)  // by name: 1059763968452780
  }

  calledByValue(System.nanoTime())
  calledByName(System.nanoTime())


  def infinite(): Int = 1 + infinite()
  def printFirst(x: Int, y: => Int) = println(x)

//  printFirst(infinite(), 34)    // StackOverflowError
  printFirst(34, infinite())

}
