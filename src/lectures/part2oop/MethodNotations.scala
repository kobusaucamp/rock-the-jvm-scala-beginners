package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/07
  * Time: 7:17 AM
  */
object MethodNotations extends App {


  class Person(val name: String, favoriteMovie: String, val age: Int = 0) {
    def likes(movie: String): Boolean = movie == favoriteMovie
    def hangOutWith(person: Person): String = s"${this.name} is hanging out with ${person.name}"

    def +(person: Person): String = s"${this.name} is hanging out with ${person.name}"
    def +(nickName: String): Person = new Person(s"$name ($nickName),", favoriteMovie)

    def unary_! : String = s"$name, what the heck?!"       // space between ! and : is important else compiler will think the colon is part of the method name
    def unary_+ : Person = new Person(name, favoriteMovie, age + 1)

    def isAlive: Boolean = true
    def learns(subject: String): String = s"$name is learning $subject"
    def learnsScala: String = this learns "Scala"

    def apply(): String = s"Hi, my name is $name and I like $favoriteMovie"
    def apply(n: Int): String = s"$name watched $favoriteMovie $n times"

  }


  val mary = new Person("Mary", "Inception")
  // the next two statements are equivalent and is an example of (syntactic sugar)
  println(mary.likes("Inception"))
  println(mary likes "Inception")    // INFIX notation = OPERATION notation => works only with methods which only have one parameter

  // "operators" in scala
  val tom = new Person("Tom", "Fight Club")

  // the following two are the same
  println(mary hangOutWith tom)    // INFIX notation = OPERATION notation
  println(mary.hangOutWith(tom))

  // we can name methods with reserved words like +/-* and they are valid
  println(mary + tom)    // INFIX notation = OPERATION notation
  println(mary.+(tom))

  // ALL OPERATORS ARE METHODS
  println(1 + 2)
  println(1.+(2))

  // prefix notation
  val x = -1   // equivalent 1.unary_-
  val y = 1.unary_-
  // unary_ prefix only works with + - ~ !
  println(!mary)
  println(mary.unary_!)

  // postfix notation  -  only available to methods without parameters
  println(mary.isAlive)
  println(mary isAlive)

  // apply
  // the following two methods are equivalent
  println(mary.apply())
  println(mary())


  println("\n")
  println("--------- Exercises -----------")
  println("\n")

  /*
    1.  - Overload the + operator
          ex.: mary + "the rockstar" => "Mary (the rockstar)"
   */
  println((mary + "the rockstar")())

  /*
    2.  - Add an age to the Person class with default value
        - Add unary + operator => new Person with age + 1
        - +mary => mary with the age incremented
   */
  println((+mary).age)

  /*
    3.  - Add a "learns" method in the Persons class that receives a string parameter => "Mary learns Scala"
        - Add a learnsScala method which doesn't receives any parameters and calls learns method with "Scala"
        - Use it in postfix notation
   */
  println(mary learns "Scala")
  println(mary learnsScala)

  /*
    4.  - Overload the apply method to receive a number and return a string
          mary.apply(2) => "Mary watch Inception 2 times"
   */

  println(mary.apply(2))
  println(mary(2))
}
