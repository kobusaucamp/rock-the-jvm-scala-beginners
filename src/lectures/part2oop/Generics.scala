package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/10
  * Time: 2:23 PM
  */
object Generics extends App {

  class MyList[A] {
    // use the type A within the class definition
    // traits can also be trait parameterized
  }

  class MyListB[+A] {
    def add[B >: A](element: B): MyListB[B] = ???
    /*
      A = Cat
      B = Dog = Animal
     */

    // this answers the hard question asked in point 1 below

    // when you add a new Dog to a list of Cats, then the list is turned into a more generic list of Animals

    // animalList.add(new Dog)  ???  HARD QUESTION   =>   we return a list of Animals
  }

  // multiple type parameters
  class MyMap[Key, Value]

  val listOfIntegers = new MyList[Int]
  val listOfStrings = new MyList[String]

  // generic methods
  object MyList {
    def empty[A]: MyList[A] = ???
  }
  val emptyListOfIntegers = MyList.empty[Int]

  // variance problem

  // if B extends A, should List[B] extend List[A]

  class Animal
  class Cat extends Animal
  class Dog extends Animal

  // Can List of Cats extend List of Animal  =>  there are 3 answers

  // 1. Yes: List[Cats] extends List[Animal] = COVARIANCE
  class CovariantList[+A]
  val animal: Animal = new Cat
  val animalList: CovariantList[Animal] = new CovariantList[Cat]

  // animalList.add(new Dog)  ???  HARD QUESTION   =>   we return a list of Animals

  // 2. No: INVARIANCE
  class InvariantList[A]
  // val invariantAnimalList: InvariantList[Animal] = new InvariantList[Cat] //(not possible)
  val invariantAnimalList: InvariantList[Animal] = new InvariantList[Animal] //(is possible)

  // 3. Hell, no!   CONTRAVARIANCE
  class ContravariantList[-A]
  val contravariantList: ContravariantList[Cat] = new ContravariantList[Animal]

  //makes more sense, just the way it reads
  class Trainer[-A]
  val trainer: Trainer[Cat] = new Trainer[Animal]


  // bounded types
  // upper bounded type in Animal
  class Cage[A <: Animal](animal: A)      // constraint/restriction   (subtypes of Animal)
  val cage = new Cage(new Dog)

//  class Car
//  val newCage = new Cage(new Car)        // not possible since only animal is bounded

  // lower bounded type of Animal
//  class Cage[A >: Animal](animal: A)      // constraint/restriction   (supertypes of Animal)
//  val cage = new Cage(new Dog)


}
