package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/10
  * Time: 7:21 AM
  */
object AbstractDataTypes extends App {

  // abstract
  abstract class Animal {
    val creatureType: String = "wild"
    def eat: Unit
  }


  class Dog extends Animal {
    override val creatureType: String = "Canine"
    def eat: Unit = println("crunch crunch")    // overriding is not strictly needed, so, if override is removed all should still be okey
  }

  // traits - the ultimate abstract data types in scala
  trait Carnivore {
    def eat(animal: Animal): Unit
    val preferredMeal: String = "fresh meat"
  }

  trait ColdBlooded

  class Crocodile extends Animal with Carnivore with ColdBlooded {
    override val creatureType: String = "croc"
    def eat: Unit = println("nom nom nom")

    def eat(animal: Animal): Unit = println(s"I'm a crock and I'm eating ${animal.creatureType}")
  }

  val dog = new Dog
  val crocodile = new Crocodile

//  crocodile.eat(dog)

  crocodile eat dog

  // traits vs abstract classes
  // 1 - traits do not have constructor parameters
  // 2 - can extend only one class but can mix in multiple traits in the same class
  // 3 - traits = behaviors (describe what they do), abstract class is a type of thing

}
