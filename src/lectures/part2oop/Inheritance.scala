package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/10
  * Time: 6:40 AM
  */
object Inheritance extends App {

  // single class inheritance
  sealed class Animal {
    val creatureType = "wild"
//    protected def eat = println("nom nom nom")
    def eat = println("nom nom nom")
  }

  class Cat extends Animal { // Cat = subclass of Animal  &  Animal = superclass of Cat
    // SUBCLASS only inherits NON-PRIVATE members of the SUPERCLASS

    def crunch = {
      eat
      println("crunch crunch")
    }
  }
  val cat = new Cat
  cat.crunch

  //constructors
  class Person(name: String, age: Int) {
    def this(name: String) = this(name, 0)
  }
  //both of the following is acceptable
  //  class Adult(name:  String, age: Int, idCard: String) extends Person(name, age)
  class Adult(name:  String, age: Int, idCard: String) extends Person(name)

  // overriding
  // can also override in the constructor...
  class Dog(override val creatureType: String = "domestic") extends Animal {
  // override val creatureType: String = "domestic"
    override def eat: Unit = {
      // super
      super.eat
      println("crunch, crunch")
    }
  }

  val dog =  new Dog("pet")
  dog.eat
  println(dog.creatureType)

  // type substitution    (broadly called    POLYMORPHISM)
  val unknownAnimal: Animal = new Dog("K9")
  unknownAnimal.eat


  // Make sure you understand the difference of overRIDING vs overLOADING

  // preventing overrides
  // 1 - use final on member
  // 2 - use final on super class
  // 3 - use sealed : seal the class = extends classes in THIS FILE ONLY but prevent extension in other files

}
