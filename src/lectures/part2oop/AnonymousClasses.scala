package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/12
  * Time: 1:16 PM
  */
object AnonymousClasses extends App {

  // AnonymousClasses works for both abstract and non-abstract classes and traits

  abstract class Animal {
    def eat: Unit
  }

  // anonymous class
  val funnyAnimal: Animal = new Animal {
    override def eat: Unit = println("ahahahahahahaha")
  }

  // This is what actually happens...
  /*
    class AnonymousClasses$$anon$1 extends Animal {
      override def eat: Unit = println("ahahahahahahaha")
    }
    val funnyAnimal: Animal = new AnonymousClasses$$anon$1
  */

  println(funnyAnimal.getClass)

  class Person(name: String) {
    def sayHi: Unit = println(s"Hi, my name is $name, how can I help you?")
  }

  val jim = new Person("Jim") {
    override def sayHi: Unit = println(s"Hi, my name is Jim, how can I be of service?")
  }

}
