package lectures.part2oop

import java.sql

import exercises.{MyList, Writer => Skrywer}    // imports both the classes in package playground be usable by it's simple name
                                                // Writer can now be referred to by Skrywer
//import exercises._ // ._ imports all the classes in package playground be usable by it's simple name (only use if actually needs it)
import playground.PrinceCharming

import java.util.Date
import java.sql.{Date => SqlDate}

/**
  * User: kobusa
  * Date: 2018/12/14
  * Time: 8:52 AM
  */
object PackagingAndImports extends App {

  // package members are accessible by their simple name
  val person = new Person("Kobus", 46)

  // import the package then use the class by the simple name
  val writer = new Skrywer("Kobus", "Aucamp", 2018)
  // or
  val stephen = new exercises.MyList {     // exercises.MyList = fully qualified name
    override def head: Int = ???

    override def tail: MyList = ???

    override def isEmpty: Boolean = ???

    override def add(element: Int): MyList = ???

    override def printElements: String = ???
  }

  // packages are in hierarchy
  // matching folder structure

  // package object
  sayHello
  println(SPEED_OF_LIGHT)

  // imports
  val prince = new PrinceCharming

  // Multi Imports with same name

  // 1. use fully qualified name
  val date = new Date

  // 2. use aliasing
  val sqlDate = new SqlDate(2018, 5, 4)


  // default import
  // java.lang - String, Object, Exception
  // scala - Int, Nothing, Function
  // scala.Predef - println, ???



}
