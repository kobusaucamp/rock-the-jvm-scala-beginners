package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/05
  * Time: 1:20 PM
  */
object OOBasics extends App {

  val person = new Person("John", 26)      //Instantiation
  println(person)
  println(person.age)      // you need to have the val or var keyword before the parameter to make it a field
  println(person.x)

  person.greet("Daniel")

}

// A class organizes data and behaviour that is code
// Constructor
// With every instantiation the whole code block of the class will be evaluated, so, when new Person is used the println(1 + 3) is being evaluated

// class parameters are NOT FIELDS (fields need val or var before it)
class Person(name: String, val age: Int = 0) {
  //body    -     defines the implementation of the class

  val x = 2     //field
  println(1 + 3)

  // method
  def greet(name: String): Unit = println(s"${this.name} says: Hi, $name")
  // vs      check this difference carefully
  def greet(): Unit = println(s"Hi, I am $name")     // overloading  -  methods with same name but different signatures.

  // multiple constructors
  def this(name: String) = this(name, 0)      // rather define a default parameter to the class definition
  def this() = this("John Doe")               // also called auxiliary constructors
}
