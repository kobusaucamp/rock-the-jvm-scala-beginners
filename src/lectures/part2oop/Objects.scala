package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/07
  * Time: 1:22 PM
  */
object Objects extends App {

  // SCALA DOES NOT HAVE CLASS-LEVEL FUNCTIONALITY ("static")
  // This is the equivalent of static in Java...
  //The object definition is defined by its type and its the only instance of this type
  object Person { //objects do not receive parameters
    // "static"/"class" - level functionality
    val N_EYES = 2

    def canFly: Boolean = false

    // factory method
    def apply(mother: Person, father: Person): Person = new Person("Bobbie")
  }

  class Person(val name: String) {
    // instance-level functionality
  }

  //object and class Person are COMPANIONS

  println(Person.N_EYES)
  println(Person.canFly)

  // Scala objects = SINGLETON INSTANCE
  val person1 = Person
  val person2 = Person
  println(person1 == person2)

  val mary = new Person("Mary")
  val john = new Person("John")
  println(mary == john)

  //  val bobbie = Person.from(mary, john)
  // OR
  val bobbie = Person.apply(mary, john)
  // OR
  val newBobbie = Person(mary, john)

  // Scala Applications = Scala object with
  //    def main(args: Array[String]): Unit { put all your code in here }


}
