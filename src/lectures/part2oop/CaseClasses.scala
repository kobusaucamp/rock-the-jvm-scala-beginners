package lectures.part2oop

/**
  * User: kobusa
  * Date: 2018/12/13
  * Time: 8:02 AM
  */
object CaseClasses extends App {

  /*
    equals, hashcode, toString
   */

  case class Person(name: String, age: Int)

  // 1. class parameters are promoted to fields
  val jim = new Person("Jim", 34)
  println(jim.name)

  // 2. sensible toString
  // println(instance) = println(instance.toString)  //syntactic sugar
  println(jim)
  println(jim.toString)

  // 3. equals and hashCode implemented out of the box (OOTB)
  val jim2 = new Person("Jim", 34)
  println(jim == jim2)    // is true because of the "case" keyword

  // 4. Have handy copy/clone method
  val jim3 = jim.copy(age = 45)
  println(jim3)

  // 5. Have companion objects
  val thePerson = Person
  val mary = Person("Mary", 23)

  // 6. Are serializable
  // Akka

  // 7. Have extractor patterns = can be used in PATTERN MATCHING

  // they do not get companion objects since they are companion objects
  case object UnitedKingdom {
    def name: String = "The UK of GB and NI"
  }

}
