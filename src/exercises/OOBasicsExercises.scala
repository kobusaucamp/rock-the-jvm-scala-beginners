package exercises

/**
  * User: kobusa
  * Date: 2018/12/06
  * Time: 11:22 AM
  */
object OOBasicsExercises extends App {

  val author = new Writer("Kobus", "Aucamp", 1972)
  println(author.fullName())

  val novel = new Novel("BookName", 2017, author)
  println(novel.authorAge)
  println(novel.isWrittenBy(author))
  println(novel.copy(2018).authorAge)


  val counter = new Counter
  counter.print
  counter.inc.print
  counter.inc.inc.inc.print

  counter.inc(10).print
}


/*
    Novel and Writer classes
    Writer: first name, surname, year of birth
      - method fullname
    Novel: name, year of release, author
      - authorAge - return age of author of year of release
      - isWrittenBy(author)
      - copy (new year of release) = new instance of Novel
   */
class Writer(firstName: String, surname: String, val year: Int) {
  def fullName(): String = s"${firstName + " " + surname}"
}

class Novel(name: String, year: Int, author: Writer) {
  def authorAge = year - author.year

  def isWrittenBy(author: Writer) = author == this.author

  def copy(newYear: Int): Novel = new Novel(name, newYear, author)
}


/*
  Counter class
    - receives and Int value
    - method returns current count
    - method to increment/decrement by one   => new Counter
    - overload inc/dec to receive an amount  => result will be a new counter
 */

class Counter(val count: Int = 0) {

  def inc = {
    println("incrementing")
    new Counter(count + 1) //immutability - since we return a new instance of the counter class
  }
  def dec = {
    println("decrementing")
    new Counter(count - 1)
  }

  def inc(n: Int): Counter = {
    if (n <= 0) this
    else inc.inc(n - 1)
  }

  def dec(n: Int): Counter = {
    if (n <= 0) this
    else dec.dec(n - 1)
  }

  def print = println(count)

}
