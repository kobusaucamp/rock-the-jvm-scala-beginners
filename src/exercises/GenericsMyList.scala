package exercises

/**
  * User: kobusa
  * Date: 2018/12/11
  * Time: 7:04 AM
  */
abstract class GenericsMyList[+A] {

  def head: A

  def tail: GenericsMyList[A]

  def isEmpty: Boolean

  def add[B >: A](element: B): GenericsMyList[B]

  def printElements: String

  // polymorphic call
  override def toString: String = "[" + printElements + "]"

  // higher-order functions
  def map[B](transformer: A => B): GenericsMyList[B]

  def flatMap[B](transformer: A => GenericsMyList[B]): GenericsMyList[B]

  def filter(predicate: A => Boolean): GenericsMyList[A]

  //concatenation
  def ++[B >: A](list: GenericsMyList[B]): GenericsMyList[B]

  // HOFs
  def foreach(f: A => Unit): Unit

  def sort(compare: (A, A) => Int): GenericsMyList[A]

  def zipWith[B, C](list: GenericsMyList[B], zip: (A, B) => C): GenericsMyList[C]

  def fold[B](start: B)(operator: (B, A) => B): B
}

case object NewEmpty extends GenericsMyList[Nothing] {

  def head: Nothing = throw new NoSuchElementException

  def tail: GenericsMyList[Nothing] = throw new NoSuchElementException

  def isEmpty: Boolean = true

  def add[B >: Nothing](element: B): GenericsMyList[B] = new NewCons(element, NewEmpty)

  def printElements: String = ""

  def map[B](transformer: Nothing => B): GenericsMyList[B] = NewEmpty

  def flatMap[B](transformer: Nothing => GenericsMyList[B]): GenericsMyList[B] = NewEmpty

  def filter(predicate: Nothing => Boolean): GenericsMyList[Nothing] = NewEmpty

  def ++[B >: Nothing](list: GenericsMyList[B]): GenericsMyList[B] = list

  // HOFs
  override def foreach(f: Nothing => Unit): Unit = ()

  override def sort(compare: (Nothing, Nothing) => Int): GenericsMyList[Nothing] = NewEmpty

  override def zipWith[B, C](list: GenericsMyList[B], zip: (Nothing, B) => C): GenericsMyList[C] =
    if (!list.isEmpty) throw new RuntimeException("Lists do not have the same length")
    else NewEmpty

  override def fold[B](start: B)(operator: (B, Nothing) => B): B = start
}

case class NewCons[+A](h: A, t: GenericsMyList[A]) extends GenericsMyList[A] {

  def head: A = h

  def tail: GenericsMyList[A] = t

  def isEmpty: Boolean = false

  def add[B >: A](element: B): GenericsMyList[B] = new NewCons(element, this)

  def printElements: String =
    if (t.isEmpty) "" + h
    else h + " " + t.printElements

  /*
    [1,2,3].filter(n % 2 == 0) =
      [2,3].filter(n % 2 == 0) =
      = new NewCons(2, [3].filter(n % 2 == 0))
      = new NewCons(2, NewEmpty.filter(n % 2 == 0))
      = new NewCons(2, NewEmpty)
   */
  def filter(predicate: A => Boolean): GenericsMyList[A] = {
    if (predicate(h)) new NewCons(h, t.filter(predicate))
    else t.filter(predicate)
  }

  /*
    [1,2,3].map(n * 2)
      = new NewCons(2, [2,3].map(n * 2))
      = new NewCons(2, new NewCons(4, [3].map(n * 2)))
      = new NewCons(2, new NewCons(4, new NewCons(6, NewEmpty.map(n * 2))))
      = new NewCons(2, new NewCons(4, new NewCons(6, NewEmpty))))
   */
  def map[B](transformer: A => B): GenericsMyList[B] = {
    new NewCons[B](transformer(h), t.map(transformer))
  }

  /*
    [1,2] ++ [3,4,5]
    = new NewCons(1, [2] ++ [3,4,5])
    = new NewCons(1, new NewCons(2, NewEmpty ++ [3,4,5]))
    = new NewCons(1, new NewCons(2, new NewCons(3, new NewCons(4, new NewCons(5)))))
   */
  def ++[B >: A](list: GenericsMyList[B]): GenericsMyList[B] = {
    new NewCons[B](h, t ++ list)
  }

  /*
    [1,2].flatMap(n => [n, n + 1])
    = [1,2] ++ [2].flatMap(n => [n, n+1])
    = [1,2] ++ [2,3] ++ NewEmpty.flatMap(n => [n, n+1])
    = [1,2] ++ [2,3] ++ newEmpty
    = [1,2,2,3]
   */
  def flatMap[B](transformer: A => GenericsMyList[B]): GenericsMyList[B] = {
    transformer(h) ++ t.flatMap(transformer)
  }

  // HOFs
  override def foreach(f: A => Unit): Unit = {
    f(h)
    t.foreach(f)
  }

  override def sort(compare: (A, A) => Int): GenericsMyList[A] = {

    def insert(x: A, sortedList: GenericsMyList[A]): GenericsMyList[A] =
      if (sortedList.isEmpty) new NewCons(x, NewEmpty)
      else if (compare(x, sortedList.head) <= 0) new NewCons(x, sortedList)
      else new NewCons(sortedList.head, insert(x, sortedList.tail))

    val sortedTail = t.sort(compare)
    insert(h, sortedTail)
  }

  override def zipWith[B, C](list: GenericsMyList[B], zip: (A, B) => C): GenericsMyList[C] = {
    if (list.isEmpty) throw new RuntimeException("Lists do not have the same length")
    else new NewCons(zip(h, list.head), t.zipWith(list.tail, zip))
  }

  override def fold[B](start: B)(operator: (B, A) => B): B = {
    t.fold(operator(start, h))(operator)
  }
}

/*
  1.  Generic trait  MyPredicate[-T] with a little method test(T)   =>  Boolean
 */

//trait MyPredicate[-T] {
//  def test(element: T): Boolean
//}

/*
  2.  Generic trait MyTransformer[-A, B] with a method transform(A) => B
*/

//trait MyTransformer[-A, B] {
//  def transform(element: A): B
//}

/*
  3.  MyList:
    - map(transformer) => MyList
    - filter(predicate) => MyList
    - flatMap(transformer from A to MyList[B]) => MyList[B]

    class EvenPredicate extends MyPredicate[Int]
    class StringToIntTransformer extends MyTransformer[String, Int]

    [1,2,3].map(n * 2) = [2,4,6]
    [1,2,3,4].filter(n % 2) = [2,4]
    [1,2,3].flatMap(n => [n, n+1]) => [1,2,2,3,3,4]
*/


object GenericsListTest extends App {

  val listOfGenericIntegers: GenericsMyList[Int] = new NewCons[Int](1, new NewCons[Int](2, new NewCons[Int](3, NewEmpty)))
  val cloneListOfGenericIntegers: GenericsMyList[Int] = new NewCons[Int](1, new NewCons[Int](2, new NewCons[Int](3, NewEmpty)))
  val anotherListOfGenericIntegers: GenericsMyList[Int] = new NewCons[Int](4, new NewCons[Int](5, NewEmpty))
  val listOfGenericStrings: GenericsMyList[String] = new NewCons[String]("Hello", new NewCons[String]("Scala", NewEmpty))

  println(listOfGenericIntegers.toString)
  println(listOfGenericStrings.toString)

  println(listOfGenericIntegers.map(elem => elem * 2).toString)
  // equivalent
  println(listOfGenericIntegers.map(_ * 2).toString)

  println(listOfGenericIntegers.filter(elem => elem % 2 == 0).toString)
  // equivalent
  println(listOfGenericIntegers.filter(_ % 2 == 0).toString)

  println((listOfGenericIntegers ++ anotherListOfGenericIntegers).toString)
  println(listOfGenericIntegers.flatMap(elem => NewCons(elem, NewCons(elem + 1, NewEmpty))).toString)

  println(cloneListOfGenericIntegers == listOfGenericIntegers)

  // HOFs
  listOfGenericIntegers.foreach(println)
  println(listOfGenericIntegers.sort((x, y) => y - x))
  println(anotherListOfGenericIntegers.zipWith[String, String](listOfGenericStrings, _ + "-" + _))

  println(listOfGenericIntegers.fold(0)(_ + _))

  // for comprehensions
  val combinations = for {
    n <- listOfGenericIntegers
    string <- listOfGenericStrings
  } yield n + "-" + string
  println(combinations)
}
